// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusDeceleration.h"
#include "SnakeBase.h"

// Sets default values
ABonusDeceleration::ABonusDeceleration()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABonusDeceleration::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ABonusDeceleration::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABonusDeceleration::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->ChangedSpeed(AccessKeys::DECELERATION);
			Destroy();
		}
	}
}

