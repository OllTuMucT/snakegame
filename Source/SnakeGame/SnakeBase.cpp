// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	StartNumberElements = 5;
	ElementSize = 75.f;
	Speed = 75;
	MinMovementSpeed = 0.7;
	MovementSpeed = 0.5;
	MaxMovementSpeed = 0.2;
	DeltaMovementSpeed = 0.05;
	LastMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(StartNumberElements + 1);
	ShowSnake();
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation((SnakeElements.Num() * ElementSize), -400, 5); //Speed
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElement->SetActorHiddenInGame(true);
		NewSnakeElement->SnakeOwner = this;
		int32 ElementIndex = SnakeElements.Add(NewSnakeElement);
		if (ElementIndex == 0)
		{
			NewSnakeElement->SetFirstElementType();
			NewSnakeElement->SetActorHiddenInGame(false);
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	//float MovementSpeed = ElementSize;

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += Speed;
	break;
	case EMovementDirection::DOWN:
		MovementVector.X -= Speed;
	break;
	case EMovementDirection::LEFT:
		MovementVector.Y -= Speed;
	break;
	case EMovementDirection::RIGHT:
		MovementVector.Y += Speed;
	break;
	}
	//AddActorWorldOffset(MovementVector);

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector); //SetActorLocation(GetActorLocation() + MovementVector)

	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::ChangedSpeed(AccessKeys Key)
{
	switch (Key)
	{
	case AccessKeys::ACCELERATION:
		if (MovementSpeed - DeltaMovementSpeed >= MaxMovementSpeed)
		{
			MovementSpeed -= DeltaMovementSpeed;
		}
	break;
	case AccessKeys::DECELERATION:
		if (MovementSpeed + DeltaMovementSpeed <= MinMovementSpeed)
		{
			MovementSpeed += DeltaMovementSpeed;
		}
	break;
	}
	SetActorTickInterval(MovementSpeed);
}

void ASnakeBase::ShowSnake()
{
	for (int i = 0; i < SnakeElements.Num() - 1; i++)
	{
		SnakeElements[i]->SetActorHiddenInGame(false);
	}
}

void ASnakeBase::HideSnake()
{
	for (int i = 0; i < SnakeElements.Num(); i++)
	{
		SnakeElements[i]->SetActorHiddenInGame(true);
	}
}
